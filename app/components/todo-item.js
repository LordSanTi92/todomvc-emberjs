import Ember from 'ember';

export default Ember.Component.extend({
  repo: Ember.inject.service(),
  tagName: 'li',
  editing: false,
  classNameBindings: [
    'todo.completed', 'editing'
  ],

  actions: {
    startEditing() {
      this.get('onStartEdit')();
      this.set('editing', true);
      Ember.run.scheduleOnce('afterRender', this, 'focusInput');
    },

    doneEditing(todoTitle) {
      if (!this.get('editing')) {
        return;
      }
      if (Ember.isBlank(todoTitle)) {
        this.send('removeTodo');
      } else {
        this.set('todo.title', todoTitle.trim());
        this.set('editing', false);
        this.get('onEndEdit')();
      }
    },

    handleKeydown(e) {
      if (e.keyCode === 13) {
        e.target.blur();
      } else if (e.keyCode === 27) {
        this.set('editing', false);
      }
    },

    toggleCompleted(e) {
      let todo = this.get('todo');
      Ember.set(todo, 'completed', e.target.checked);
      this.get('repo').persist();
      if (e.target.checked) {
        if ($(e.target).parent().find(".date").css("display") == "none") {
          $(e.target).next().css("color", "#d9d9d9");
          $(e.target).parent().find(".calendar").show();
        } else {
          if ($(e.target).parent().find(".date").val() != "") {
            $(e.target).next().css("color", "red");
            $(e.target).parent().find(".date").show();
            $(e.target).parent().find(".calendar").hide();
          } else {
            $(e.target).next().css("color", "black");
            $(e.target).parent().find(".date").hide();
            $(e.target).parent().find(".calendar").show();
          }
        }
      } else {
        if ($(e.target).parent().find(".date").css("display") == "none") {
          $(e.target).next().css("color", "black");
        } else {
          if ($(e.target).parent().find(".date").val() != "") {
            $(e.target).next().css("color", "red");
          }
        }
      }
    },

    removeTodo() {
      this.get('repo').delete(this.get('todo'));
    },
    showInput(e) {
      $(e.target).next().show();
      $(e.target).hide();

    },
    checkDate(e) {
      let RegTest = new RegExp(/^\d{4}-\d{2}-\d{2}$/);
      if (RegTest.test(e.target.value)) {
        $(e.target).parent().find("label").css("color", "red");
        $(e.target).attr("value", e.target.value)
				$(e.target).css("backgroundColor","white");
      } else {
        $(".view").find("label").css("color", "black");
				$(e.target).css("backgroundColor","red");
				alert("Please check if the date entered is in a proper format YYYY-MM-DD");
      }
    }
  },

  focusInput() {
    this.element.querySelector('input.edit').focus();
  }
});
